import { Component, OnInit } from '@angular/core';
import { FoodService } from '../services/food/food.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  foods:string[]= [];
  constructor(private fs:FoodService) { }

  ngOnInit(): void {
    this.foods = this.fs.getAll();
  }

}
